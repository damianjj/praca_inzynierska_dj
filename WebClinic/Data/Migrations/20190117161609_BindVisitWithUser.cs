﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebClinic.Data.Migrations
{
    public partial class BindVisitWithUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Visit");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Visit");

            migrationBuilder.DropColumn(
                name: "PeselNo",
                table: "Visit");

            migrationBuilder.AddColumn<string>(
                name: "Patient",
                table: "Visit",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Visit_Patient",
                table: "Visit",
                column: "Patient");

            migrationBuilder.AddForeignKey(
                name: "FK_Visit_AspNetUsers_Patient",
                table: "Visit",
                column: "Patient",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Visit_AspNetUsers_Patient",
                table: "Visit");

            migrationBuilder.DropIndex(
                name: "IX_Visit_Patient",
                table: "Visit");

            migrationBuilder.DropColumn(
                name: "Patient",
                table: "Visit");

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Visit",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Visit",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "PeselNo",
                table: "Visit",
                nullable: false,
                defaultValue: 0);
        }
    }
}
