﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebClinic.Data.Migrations
{
    public partial class BindVisitWithUserCorrection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Visit_AspNetUsers_Patient",
                table: "Visit");

            migrationBuilder.RenameColumn(
                name: "Patient",
                table: "Visit",
                newName: "PatientId");

            migrationBuilder.RenameIndex(
                name: "IX_Visit_Patient",
                table: "Visit",
                newName: "IX_Visit_PatientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Visit_AspNetUsers_PatientId",
                table: "Visit",
                column: "PatientId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Visit_AspNetUsers_PatientId",
                table: "Visit");

            migrationBuilder.RenameColumn(
                name: "PatientId",
                table: "Visit",
                newName: "Patient");

            migrationBuilder.RenameIndex(
                name: "IX_Visit_PatientId",
                table: "Visit",
                newName: "IX_Visit_Patient");

            migrationBuilder.AddForeignKey(
                name: "FK_Visit_AspNetUsers_Patient",
                table: "Visit",
                column: "Patient",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
