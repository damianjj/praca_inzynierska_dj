﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebClinic.Data.Migrations
{
    public partial class BindRoleWithUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "WebClinicUserId",
                table: "AspNetRoles",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoles_WebClinicUserId",
                table: "AspNetRoles",
                column: "WebClinicUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetRoles_AspNetUsers_WebClinicUserId",
                table: "AspNetRoles",
                column: "WebClinicUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetRoles_AspNetUsers_WebClinicUserId",
                table: "AspNetRoles");

            migrationBuilder.DropIndex(
                name: "IX_AspNetRoles_WebClinicUserId",
                table: "AspNetRoles");

            migrationBuilder.DropColumn(
                name: "WebClinicUserId",
                table: "AspNetRoles");
        }
    }
}
