﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebClinic.Models;

namespace WebClinic.Data
{
    public class ApplicationDbContext : IdentityDbContext<WebClinicUser, IdentityRole, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<WebClinic.Models.Treatment> Treatment { get; set; }
        public DbSet<WebClinic.Models.Visit> Visit { get; set; }
    }
}
