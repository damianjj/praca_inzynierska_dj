﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

(function ($) {
    $(function () {
        $('input.timepicker').timepicker({
            timeFormat: 'HH:mm',
            interval: 30,
            minTime: '8:00',
            maxTime: '16:00',
            defaultTime: '8:00',
            startTime: '8:00',
            dynamic: false,
            dropdown: true,
            scrollbar: false
        });
    });
})(jQuery);

$(function () {
    $('[data-patient-menu]').hover(function () {
        $('[data-patient-menu]').toggleClass('open');
    })
})

$(function () {
    $('[data-doctor-menu]').hover(function () {
        $('[data-doctor-menu]').toggleClass('open');
    })
})

$(function () {
    $('[data-admin-menu]').hover(function () {
        $('[data-admin-menu]').toggleClass('open');
    })
})