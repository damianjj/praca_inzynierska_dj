﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebClinic.Data;
using WebClinic.Models;

namespace WebClinic.Pages.Visits
{
    [Authorize(Policy = RoleTypes.PatientEndUser)]
    public class CreateModel : PageModel
    {
        private readonly UserManager<WebClinicUser> _userManager;
        private readonly ApplicationDbContext _context;

        public CreateModel(UserManager<WebClinicUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public IActionResult OnGet()
        {
            ViewData["TreatmentId"] = new SelectList(_context.Set<Treatment>(), "Id", "Name");
            ViewData["DoctorId"] = new SelectList(_context.Set<WebClinicUser>().Where(u => u.Role.ToString().Equals(RoleTypes.DoctorEndUser)), "Id", "FullName");

            return Page();
        }

        [BindProperty]
        public Visit Visit { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Visit.WebClinicPatient = await _userManager.GetUserAsync(User);

            Visit.DateTime = new DateTime(
                Int32.Parse(Visit.DateOnly.Substring(0, 4)), 
                Int32.Parse(Visit.DateOnly.Substring(5, 2)), 
                Int32.Parse(Visit.DateOnly.Substring(8, 2)),
                Int32.Parse(Visit.TimeOnly.Substring(0, 2)),
                Int32.Parse(Visit.TimeOnly.Substring(3, 2)),
                0);

            _context.Visit.Add(Visit);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}