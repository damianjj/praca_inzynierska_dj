﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebClinic.Data;
using WebClinic.Models;

namespace WebClinic.Pages.Visits
{
    [Authorize(Policy = RoleTypes.PatientEndUser)]
    public class EditModel : PageModel
    {
        private readonly UserManager<WebClinicUser> _userManager;
        private readonly ApplicationDbContext _context;

        public EditModel(UserManager<WebClinicUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        [BindProperty]
        public Visit Visit { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Visit = await _context.Visit
                .Include(v => v.Treatment)
                .Include(v => v.WebClinicDoctor)
                .Include(v => v.WebClinicPatient)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (Visit == null)
            {
                return NotFound();
            }

            ViewData["TreatmentId"] = new SelectList(_context.Set<Treatment>(), "Id", "Name");
            ViewData["DoctorId"] = new SelectList(_context.Set<WebClinicUser>(), "Id", "FullName");

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Visit).State = EntityState.Modified;

            try
            {
                Visit.WebClinicPatient = await _userManager.GetUserAsync(User);

                Visit.DateTime = new DateTime(
                    Int32.Parse(Visit.DateOnly.Substring(0, 4)),
                    Int32.Parse(Visit.DateOnly.Substring(5, 2)),
                    Int32.Parse(Visit.DateOnly.Substring(8, 2)),
                    Int32.Parse(Visit.TimeOnly.Substring(0, 2)),
                    Int32.Parse(Visit.TimeOnly.Substring(3, 2)),
                    0);

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VisitExists(Visit.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool VisitExists(int id)
        {
            return _context.Visit.Any(e => e.Id == id);
        }
    }
}
