﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebClinic.Data;
using WebClinic.Models;

namespace WebClinic.Pages.Visits
{
    [Authorize(Policy = RoleTypes.PatientEndUser)]
    public class IndexModel : PageModel
    {
        private readonly UserManager<WebClinicUser> _userManager;
        private readonly ApplicationDbContext _context;

        public IndexModel(UserManager<WebClinicUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public IList<Visit> Visit { get;set; }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);

            Visit = await _context.Visit
                .Where(v => v.WebClinicPatient.Equals(user))
                .Include(v => v.Treatment)
                .Include(v => v.WebClinicDoctor)
                .Include(v => v.WebClinicPatient)
                .OrderBy(v => v.DateTime)
                .ToListAsync();

            return Page();
        }
    }
}
