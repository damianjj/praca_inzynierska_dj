﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebClinic.Models;

namespace WebClinic.Pages.DoctorVisits
{
    [Authorize(Policy = RoleTypes.DoctorEndUser)]
    public class CreateModel : PageModel
    {
        private readonly WebClinic.Data.ApplicationDbContext _context;

        public CreateModel(WebClinic.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["TreatmentId"] = new SelectList(_context.Treatment, "Id", "Name");
        ViewData["DoctorId"] = new SelectList(_context.Users, "Id", "Id");
        ViewData["PatientId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public Visit Visit { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Visit.Add(Visit);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}