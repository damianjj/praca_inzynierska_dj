﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebClinic.Data;
using WebClinic.Models;

namespace WebClinic.Pages.ManageUsers
{
    [Authorize(Policy = RoleTypes.AdminEndUser)]
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public IndexModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<WebClinicUser> WebClinicUser { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            WebClinicUser = await _context.Users
                .Include(u => u.Role)
                .OrderByDescending(u => u.Role)
                .ToListAsync();

            return Page();
        }
    }
}
