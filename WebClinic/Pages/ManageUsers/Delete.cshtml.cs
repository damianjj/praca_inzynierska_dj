﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebClinic.Data;
using WebClinic.Models;

namespace WebClinic.Pages.ManageUsers
{
    [Authorize(Policy = RoleTypes.AdminEndUser)]
    public class DeleteModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public DeleteModel(ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public WebClinicUser WebClinicUser { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id.Equals(null))
            {
                return NotFound();
            }

            WebClinicUser = await _context.Users
                .Include(v => v.Role)
                .FirstOrDefaultAsync(m => m.Id.Equals(id));

            if (WebClinicUser == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string id)
        {
            if (id.Equals(null))
            {
                return NotFound();
            }

            WebClinicUser = await _context.Users.FindAsync(id);

            if (WebClinicUser != null)
            {
                _context.Users.Remove(WebClinicUser);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
