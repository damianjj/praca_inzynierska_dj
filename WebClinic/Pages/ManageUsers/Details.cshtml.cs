﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebClinic.Data;
using WebClinic.Models;

namespace WebClinic.Pages.ManageUsers
{
    [Authorize(Policy = RoleTypes.AdminEndUser)]
    public class DetailsModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public DetailsModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public WebClinicUser WebClinicUser { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id.Equals(null))
            {
                return NotFound();
            }

            WebClinicUser = await _context.Users
                .Include(u => u.Role)
                .FirstOrDefaultAsync(m => m.Id.Equals(id));

            if (WebClinicUser.Equals(null))
            {
                return NotFound();
            }

            return Page();
        }
    }
}
