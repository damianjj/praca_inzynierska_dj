﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebClinic.Models;

namespace WebClinic.Pages.Treatments
{
    [Authorize(Policy = RoleTypes.AdminEndUser)]
    public class IndexModel : PageModel
    {
        private readonly WebClinic.Data.ApplicationDbContext _context;

        public IndexModel(WebClinic.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Treatment> Treatment { get;set; }

        public async Task OnGetAsync()
        {
            Treatment = await _context.Treatment.ToListAsync();
        }
    }
}
