﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebClinic.Models;

namespace WebClinic.Pages.Treatments
{
    [Authorize(Policy = RoleTypes.AdminEndUser)]
    public class DetailsModel : PageModel
    {
        private readonly WebClinic.Data.ApplicationDbContext _context;

        public DetailsModel(WebClinic.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Treatment Treatment { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Treatment = await _context.Treatment.FirstOrDefaultAsync(m => m.Id == id);

            if (Treatment == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
