﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebClinic.Models
{
    public class WebClinicUser : IdentityUser
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Display(Name = "PESEL number")]
        public string PeselNo { get; set; }

        [NotMapped]
        [Display(Name = "Full name")]
        public string FullName { get { return FirstName + " " + LastName; } }

        [Display(Name = "Role")]
        public string RoleId { get; set; }

        [ForeignKey("RoleId")]
        public virtual IdentityRole Role { get; set; }
    }
}
