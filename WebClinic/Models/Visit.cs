﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebClinic.Models
{
    public class Visit
    {
        public int Id { get; set; }

        [Display(Name = "Patient")]
        public string PatientId { get; set; }

        [ForeignKey("PatientId")]
        public virtual WebClinicUser WebClinicPatient { get; set; }

        [Required]
        public DateTime DateTime { get; set; }

        [Required]
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        public string DateOnly { get; set; }

        [Required]
        [Display(Name = "Time")]
        [DataType(DataType.Time)]
        public string TimeOnly { get; set; }

        [Display(Name = "Treatment")]
        public int TreatmentId { get; set; }

        [ForeignKey("TreatmentId")]
        public virtual Treatment Treatment { get; set; }

        [Display(Name = "Doctor")]
        public string DoctorId { get; set; }

        [ForeignKey("DoctorId")]
        public virtual WebClinicUser WebClinicDoctor { get; set; }

        [Required]
        [Display(Name = "Delay (mins)")]
        public int Delay { get; set; }
    }
}
