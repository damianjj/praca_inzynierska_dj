﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebClinic.Models
{
    public class RoleTypes
    {
        public const string AdminEndUser = "Admin";
        public const string DoctorEndUser = "Doctor";
        public const string PatientEndUser = "Patient";
    }
}
