﻿using System.ComponentModel.DataAnnotations;

namespace WebClinic.Models
{
    public enum DelayOptions
    {
        [Display(Name = "0")]
        zero = 0,
        [Display(Name = "5")]
        five = 5,
        [Display(Name = "10")]
        ten = 10,
        [Display(Name = "15")]
        fifteen = 15,
        [Display(Name = "20")]
        twenty = 20,
        [Display(Name = "25")]
        twentyfive = 25,
        [Display(Name = "30")]
        thirty = 30,
        [Display(Name = "35")]
        thirtyfivee = 35,
        [Display(Name = "40")]
        fourty = 40,
        [Display(Name = "45")]
        fourtyfive = 45,
        [Display(Name = "50")]
        fifty = 50,
        [Display(Name = "55")]
        fiftyfive = 55,
        [Display(Name = "60")]
        sixty = 60
    }
}
