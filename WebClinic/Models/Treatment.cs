﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebClinic.Models
{
    public class Treatment
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Treatment")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        [Display(Name = "Cost")]
        public float Cost { get; set; }

        [Required]
        [Display(Name = "DelayProbability")]
        public int DelayProbability { get; set; }
    }
}
